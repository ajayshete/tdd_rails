class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string "first_name",:limit=>25
      t.string "last_name",:limit=>25
      t.integer "mobile_no",:limit=>10
      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end
