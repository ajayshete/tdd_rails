class ChangeUser < ActiveRecord::Migration[6.0]
  def up
    rename_table("users","admin_users")
    add_column("admin_users","username",:string,:limit=>25,:after=>"mobile_no")
    add_index("admin_users","username")
  end

  def down
    remove_index("admin_users","username")
        remove_column("admin_users","username")
          rename_table("admin_users","users")
  end
end
