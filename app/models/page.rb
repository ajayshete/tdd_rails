class Page < ApplicationRecord

  belongs_to :subject

validates :subject_id ,presence: true
validates :name ,presence: true
validates :peralink ,presence: true
validates :position ,presence: true

  scope :visible_subjects, lambda {where(:visible => true)}
  scope :invisible_subjects, lambda {where(:visible => false)}
  scope :sorted, lambda {order("position ASC")}
  scope :newest_first, lambda {order("created_at DESC")}
  scope :search, lambda {|query| where(["name LIKE ?","%#{query}%"])}


end
