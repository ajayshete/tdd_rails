class Section < ApplicationRecord

validates :page_id ,presence: true
validates :name ,presence: true
validates :position ,presence: true

validates :content_type ,presence: true
validates :content ,presence: true
end
