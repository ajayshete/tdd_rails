class Subject < ApplicationRecord

has_many :pages

 validates :name ,presence: true
 validates :position ,presence: true


  scope :visible_subjects, lambda {where(:visible => true)}
  scope :invisible_subjects, lambda {where(:visible => false)}
  scope :sorted, lambda {order("position ASC")}
  scope :search, lambda {|query| where(["name LIKE ?","%#{query}%"])}

end
