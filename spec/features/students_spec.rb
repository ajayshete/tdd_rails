require 'rails_helper'

RSpec.feature "Subjects", type: :feature do
  context 'create new subject' do
    before(:each) do
     visit new_subject_path
      within('form') do
        fill_in 'subject_name', with:'math'

      end
    end
    scenario "should be successful" do
      within('form') do
       fill_in 'subject_position', with:'1'
     end
      click_button 'Create Subject'
      expect(page).to have_content('Subject created successfully......')
    end


    scenario  'should fail' do
      click_button 'Create Subject'
      expect(page).to have_content("BACK TO LIST")
    end
  end

  context 'update Subject' do
     let!(:subject){Subject.create(name:'math',position:'1')}
      before(:each) do
        visit edit_subject_path(subject)
      end
    scenario "should be successful" do
        within('form') do
       fill_in 'subject_position', with:'1'
     end
      click_button 'Update Subject'
      expect(page).to have_content 'Subject updated successfully......'
      expect(page).to have_content '1'
    end
  scenario "should fail" do
    within("form") do
      fill_in 'subject_name',with: ''
    end
          click_button 'Update Subject'
      expect(page).to have_content("BACK TO LIST")

    end
  end
  context 'destroy subject' do
    scenario "should be successful" do
      subject=Subject.create(name:'math',position:'1')
      visit subjects_path
       expect(page).to have_content '1'
      end
    end
end