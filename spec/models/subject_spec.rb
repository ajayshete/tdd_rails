require 'rails_helper'

RSpec.describe Subject, type: :model do
    context 'validation tests' do

    it 'ensure subject name present' do
      subject = Subject.new(position: '1').save
        expect(subject).to eq(false)
    end
    it 'ensure position present' do
       subject = Subject.new(name: 'math').save
        expect(subject).to eq(false)
    end

    it 'should save successfully' do
        subject = Subject.new(name: 'math',position:'1').save
        expect(subject).to eq(true)
    end

  end

  context 'scope tests' do

    let(:params){{name: 'math',position:'1'}}

    before(:each) do
      Subject.new(params).save
      Subject.new(params).save
      Subject.new(params.merge({visible:false})).save
      Subject.new(params.merge({visible:true})).save
      Subject.new(params.merge({visible:true})).save
      end

      it 'should return visible subjects' do
        expect(Subject.visible_subjects.size).to eq(2)
      end

      it 'should return invisible subjects' do
        expect(Subject.invisible_subjects.size).to eq(3)
      end

   end

end
