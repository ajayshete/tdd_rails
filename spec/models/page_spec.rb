require 'rails_helper'

RSpec.describe Page, type: :model do
    context 'validation tests' do
    it 'ensure subject id present' do
      page= Page.new(name:'math',peralink:'link',position: '1').save
        expect(page).to eq(false)
    end
    it 'ensure position present' do
      page= Page.new(subject_id: '1',name:'math',peralink:'link').save
        expect(page).to eq(false)
    end

    it 'ensure subject name present' do
        page= Page.new(subject_id: '1',peralink:'link',position: '1').save
        expect(page).to eq(false)
    end
    it 'ensure peralink present' do
        page= Page.new(subject_id: '1',name:'math',position: '1').save
        expect(page).to eq(false)
    end
    it 'should save successfully' do
        page= Page.new(subject_id: '1',name:'math',peralink:'link',position: '1').save
        expect(page).to eq(true)
    end
  end
end

