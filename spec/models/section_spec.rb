require 'rails_helper'

RSpec.describe Section, type: :model do

    context 'validation tests' do

    it 'ensure page id present' do
     section= Section.new(name:'math',position: '1',visible: 'false',content_type:'xyz',content:'text').save
        expect(section).to eq(false)
    end
    it 'ensure position present' do
      section= Section.new(page_id: '1',name:'math',visible: 'false',content_type:'xyz',content:'text').save
        expect(section).to eq(false)
    end
    it 'ensure name present' do
      section= Section.new(page_id: '1',position: '1',content_type:'xyz',content:'text').save
        expect(section).to eq(false)
    end

    it 'ensure content_type present' do
        section= Section.new(page_id: '1',name:'math',position: '1',content:'text').save
        expect(section).to eq(false)
    end
    it 'ensure content present' do
       section= Section.new(page_id: '1',name:'math',position: '1',content_type:'xyz').save
        expect(section).to eq(false)
    end
    it 'should save successfully' do
        section= Section.new(page_id: '1',name:'math',position: '1',content_type:'xyz',content:'text').save
        expect(section).to eq(true)
    end

  end
end
