require 'rails_helper'

RSpec.describe "subjects/show.html.erb", type: :view do
  it "display the subjects" do
    subject =Subject.create!(:name =>'math',:position => '1')
    controller.extra_params ={:id =>subject.id}

    expect(controller.request.fullpath).to eq subject_path(subject)
  end
end