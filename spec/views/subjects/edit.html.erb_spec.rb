require 'rails_helper'

RSpec.describe "subjects/edit.html.erb", type: :view do

  before(:each) do
    @subject = assign(:subject, Subject.create!(:name =>'math',:position => '1'))
  end

  it "renders edit form" do
    stub_template "subjects/edit.html.erb" => "Name"
    render
    expect(rendered).to match /Name/
  end
end

