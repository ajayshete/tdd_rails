require 'rails_helper'

RSpec.describe "subjects/index.html.erb", type: :view do

  it "display all the subjects" do
    assign(:subjects,[Subject.create!(:name => 'math',:position =>'1')])

    render

    expect(rendered).to match('math')
    expect(rendered).to match('1')
  end
end
