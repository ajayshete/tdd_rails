require 'rails_helper'

RSpec.describe "subjects/new.html.erb", type: :view do

  it "infers the controller action" do
    subject =Subject.create!(:name =>'math',:position => '1')
    controller.extra_params ={:id =>subject.id}
    expect(controller.request.path_parameters[:action]).to eq("new")
  end
end
